package br.com.ads.cadastro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Creditos extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.creditos);
		
		Button btVoltar = (Button)findViewById(R.id.btTelaPrincipal);
		TextView tvFuncao = (TextView)findViewById(R.id.tvFuncao);
		
		String creditos = "\n\nDESENVOLVEDORES:\n\nJoão Lima Neto\nMatrícula: 201107023191;\nEmail: joaonetoce@hotmail.com\n\n" +
				"Francisco Alex Xavier de Lima\nMatrícula: 201107022991\nEmail: alex.xavier.lima@gmail.com\n\n" +
		         "*** CENTRO UNIVERSITÁRIO ESTÁCIO/FIC ***\n" +
				 "CURSO: Análise e Desenvolvimento de Sistemas\n" +
		         "Programação para Dispositivos Móveis\n" +
				 "Prof.: Milton Escossia";
		
		tvFuncao.setText(creditos);
		
		btVoltar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Creditos.this, TelaPrincipalActivity.class);
				startActivity(intent);
				finish();
			}
		});
		
	}
}
