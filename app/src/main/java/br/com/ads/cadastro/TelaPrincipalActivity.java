package br.com.ads.cadastro;

import br.com.ads.cadastro.R;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class TelaPrincipalActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.telaprincipal);
		
		Button btListaAlunosADS = (Button)findViewById(R.id.btListaAlunosADS);
		Button btSobre = (Button)findViewById(R.id.btSobre);
		Button btCreditos = (Button)findViewById(R.id.btCreditos);
				
		btListaAlunosADS.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(TelaPrincipalActivity.this, ListaAlunosADSActivity.class);
				startActivity(intent);
				finish();
			}
		});
		
		btSobre.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				Intent intent = new Intent(TelaPrincipalActivity.this, Help.class);
				startActivity(intent);				
			}
		});
		
		btCreditos.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				Intent intent = new Intent(TelaPrincipalActivity.this, Creditos.class);
				startActivity(intent);
			}
		});
	}	
}
