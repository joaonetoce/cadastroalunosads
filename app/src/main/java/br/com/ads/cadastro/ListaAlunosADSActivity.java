package br.com.ads.cadastro;

import java.util.List;

import br.com.ads.cadastro.R;
import br.com.ads.cadastro.dao.AlunoDao;
import br.com.ads.cadastro.modelo.Aluno;
import br.com.ads.cadastro.util.MensagensGeral;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class ListaAlunosADSActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listaalunosads);
		
		carregarListaAlunos();
		
		Button btTelaPrincipal = (Button)findViewById(R.id.btTelaPrincipal);
		
		btTelaPrincipal.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ListaAlunosADSActivity.this, TelaPrincipalActivity.class);
				startActivity(intent);
				finish();
			}
		});		
	}
	
	@Override
	protected void onRestart() {
		carregarListaAlunos();
		super.onRestart();
	}
	
	public void carregarListaAlunos(){
		AlunoDao dao = new AlunoDao(this);
		final List<Aluno> alunos = dao.listarTodosAlunos();
		ArrayAdapter<Aluno> adapter = new ArrayAdapter<Aluno>(this, android.R.layout.simple_list_item_1, alunos);
		
		ListView listView = (ListView)findViewById(R.id.listaAlunos);
		listView.setAdapter(adapter);
		listView.setClickable(true);
		
		//TextView listaVazia = (TextView)findViewById(R.id.textViewListaVazia);
		if(adapter.isEmpty()){
			//listaVazia.setText("A LISTA DE ALUNOS N�O POSSUI NENHUM ALUNO CADASTRADO");
			MensagensGeral.exibirFeedback(this, "A LISTA ESTÁ VAZIA");
		}
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int posicao,
					long id) {
				//Toast.makeText(ListaAlunosADSActivity.this, "ID: " + alunos.get(posicao).getId(), Toast.LENGTH_LONG).show();
				Bundle parametros = new Bundle();
				parametros.putInt("id", alunos.get(posicao).getId());
				parametros.putString("nome", alunos.get(posicao).getNome());
				parametros.putString("email", alunos.get(posicao).getEmail());
				
				Intent intent = new Intent(ListaAlunosADSActivity.this, EditarAlunoADSActivity.class);
				intent.putExtras(parametros);				
				startActivity(intent);
			}			
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.tela_principal, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == R.id.adicionarAlunoADS){
			Intent intent = new Intent(this, FormularioActivity.class);
			startActivity(intent);
		}
		return super.onOptionsItemSelected(item);
	}	
}
