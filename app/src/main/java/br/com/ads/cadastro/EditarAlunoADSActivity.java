package br.com.ads.cadastro;

import br.com.ads.cadastro.R;
import br.com.ads.cadastro.dao.AlunoDao;
import br.com.ads.cadastro.modelo.Aluno;
import br.com.ads.cadastro.util.MensagensGeral;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class EditarAlunoADSActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.alterarouexcluir);
		
		Button btEditarDados = (Button)findViewById(R.id.botaoAlterar);
		Button btExcluirAluno = (Button)findViewById(R.id.botaoExcluir);
		Button btCancelar = (Button)findViewById(R.id.botaoCancelar);
		
		final EditText etNome = (EditText)findViewById(R.id.editNome);
		final EditText etEmail = (EditText)findViewById(R.id.editEmail);
		
		Intent intent = getIntent();
		Bundle parametrosRecebidos = intent.getExtras();
		
		final int id = parametrosRecebidos.getInt("id");
		final String nome = parametrosRecebidos.getString("nome");
		final String email = parametrosRecebidos.getString("email");
		
		//Log.i("Nome recebido", nome);
		//Log.i("Email recebido", email);
		
		etNome.setText(nome);
		etEmail.setText(email);
		
		btEditarDados.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try {
					Aluno aluno = new Aluno();
					aluno.setId(id);
					aluno.setNome(etNome.getEditableText().toString());
					aluno.setEmail(etEmail.getEditableText().toString());
					
					AlunoDao dao = new AlunoDao(EditarAlunoADSActivity.this);
					dao.editarAluno(aluno);
					
					boolean execMSG = MensagensGeral.exibirFeedback(EditarAlunoADSActivity.this, "ALTERADO COM SUCESSO");
					if(execMSG == true){
						finish();
					}
				} catch (Exception e) {
					MensagensGeral.exibirMensagem(EditarAlunoADSActivity.this, "ERROR", "Erro ao ALTERAR os dados do aluno!");
				}
			}
		});
		
		btExcluirAluno.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try {
					Aluno aluno = new Aluno();
					aluno.setId(id);
					aluno.setNome(nome);
					aluno.setEmail(email);
					
					AlunoDao dao = new AlunoDao(EditarAlunoADSActivity.this);
					dao.deletarAluno(aluno);
					
					boolean execMSG = MensagensGeral.exibirFeedback(EditarAlunoADSActivity.this, "EXCLUÍDO COM SUCESSO");
					if(execMSG == true){
						finish();
					}
				} catch (Exception e) {
					MensagensGeral.exibirMensagem(EditarAlunoADSActivity.this, "ERROR", "Erro ao EXCLUIR o aluno!");
				}
				
			}
		});
		
		btCancelar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(EditarAlunoADSActivity.this, ListaAlunosADSActivity.class);
				startActivity(intent);
				finish();
			}
		});
	}	
}
