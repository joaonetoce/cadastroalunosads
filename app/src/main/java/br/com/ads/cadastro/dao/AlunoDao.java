package br.com.ads.cadastro.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.ads.cadastro.modelo.Aluno;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AlunoDao extends SQLiteOpenHelper {
			
	private static int VERSION = 1;
	private static String TABELA = "Aluno";
	private static String COLUNAS[] = {"id", "nome", "email"};

	public AlunoDao(Context context) {
		super(context, AlunoDao.TABELA, null, AlunoDao.VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			String sql = "CREATE TABLE " + AlunoDao.TABELA + "(id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT(40) UNIQUE NOT NULL, email TEXT(40));";
			db.execSQL(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		try {
			db.execSQL("DROP TABLE IF EXISTS " + AlunoDao.TABELA);
			this.onCreate(db);
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	
	public void adicionarAluno(Aluno aluno){
		try {
			ContentValues values = new ContentValues();
			values.put("nome", aluno.getNome());
			values.put("email", aluno.getEmail());
			
			getWritableDatabase().insert(AlunoDao.TABELA, null, values);
		} catch (Exception e) {
			e.printStackTrace();			
		}finally{
			close();
		}
	}
	
	public List<Aluno> listarTodosAlunos(){
		List<Aluno> lista = new ArrayList<Aluno>();
		Cursor c = getWritableDatabase().query(AlunoDao.TABELA, AlunoDao.COLUNAS, null, null, null, null, null);
		try {			
			//{"id", "nome", "email"}
			//  0       1       2 
			while(c.moveToNext()){
				Aluno a = new Aluno();
				a.setId(c.getInt(0));
				a.setNome(c.getString(1));
				a.setEmail(c.getString(2));
				
				lista.add(a);
			}
			return lista;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			c.close();
		}		
	}
	
	public boolean editarAluno(Aluno aluno){
		try {
			ContentValues values = new ContentValues();
			values.put("nome", aluno.getNome());
			values.put("email", aluno.getEmail());
			
			int linha = getWritableDatabase().update(AlunoDao.TABELA, values, "id = ?", new String[] {aluno.getId().toString()});
			
			if(linha > 0){
				return true;
			}else{
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally{
			close();
		}
	}
	
	public void deletarAluno(Aluno aluno){
		try {
			Integer id = aluno.getId();
			
			getWritableDatabase().delete(AlunoDao.TABELA, "id = " + id, null);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			close();
		}
	}	
}
