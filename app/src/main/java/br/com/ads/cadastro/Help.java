package br.com.ads.cadastro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Help extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help);
		
		Button btVoltar = (Button)findViewById(R.id.btTelaPrincipal);
		TextView tvFuncao = (TextView)findViewById(R.id.tvFuncao);
		
		String sobre = "\n - CADASTRO DE ALUNOS DO CURSO DE ADS - \n" +
				"Esta aplicação tem como finalidade cadastrar " +
				"o NOME e o EMAIL dos alunos do curso de ADS.\n\n" +
				" *** MANUAL DA APLICAÇÃO *** \n" +
				"O botão 'Lista de alunos' direcionar o usuário para a tela " +
				"que lista todos os alunos cadastrados;\n\n" +
				"Na tela de listagem dos alunos, através do botão 'NOVO ALUNO' " +
				"no menu superior direito é possível adicionar um novo aluno na lista;\n\n" +
				"Clicando em qualquer aluno cadastrado da lista o usuário é "  +
				"direcionado para a tela de 'EDIÇÃO', onde É possível ALTERAR os " +
				"dados do aluno ou EXCLUIR o mesmo;";
		
		tvFuncao.setText(sobre);
		
		btVoltar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Help.this, TelaPrincipalActivity.class);
				startActivity(intent);
				finish();
			}
		});
		
	}
}
