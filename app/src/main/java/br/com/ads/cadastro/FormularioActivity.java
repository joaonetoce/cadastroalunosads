package br.com.ads.cadastro;

import br.com.ads.cadastro.R;
import br.com.ads.cadastro.dao.AlunoDao;
import br.com.ads.cadastro.modelo.Aluno;
import br.com.ads.cadastro.util.MensagensGeral;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class FormularioActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.formulario);		
		
		Button btCadastrar = (Button)findViewById(R.id.botaoCadastrar);
		btCadastrar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try {
					EditText etNome = (EditText)findViewById(R.id.editNome);
					EditText etEmail = (EditText)findViewById(R.id.editEmail);

					Aluno aluno = new Aluno();
					aluno.setNome(etNome.getEditableText().toString());
					aluno.setEmail(etEmail.getEditableText().toString());

					AlunoDao dao = new AlunoDao(FormularioActivity.this);
					dao.adicionarAluno(aluno);
					boolean execMSG = MensagensGeral.exibirFeedback(FormularioActivity.this, "CADASTRADO COM SUCESSO");
					if(execMSG == true){
						finish();
					}
				} catch (Exception e) {
					MensagensGeral.exibirMensagem(FormularioActivity.this, "ERROR", "Erro ao CADASTRAR o aluno!");									
				}			
			}
		});		
				
		Button btCancelar = (Button)findViewById(R.id.botaoCancelar);
		btCancelar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(FormularioActivity.this, ListaAlunosADSActivity.class);
				startActivity(intent);
				finish();
			}
		});
	}
}
