package br.com.ads.cadastro.util;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.widget.Toast;

public class MensagensGeral {

	public static boolean exibirMensagem(Context context, String titulo, String mensagem){
		
			Builder caixaDeMensagem = new Builder(context);
			caixaDeMensagem.setTitle(titulo);
			caixaDeMensagem.setMessage(mensagem);
			caixaDeMensagem.setNeutralButton("Ok", null);
			caixaDeMensagem.show();
			
			return true;		
	}	
	
	public static  boolean exibirFeedback(Context context, String mensagem){
		Toast toast = Toast.makeText(context, mensagem, Toast.LENGTH_LONG);
		toast.setGravity(0, 0, 0);
		toast.show();
		return true;
	}	
}
